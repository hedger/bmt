// ==UserScript==
// @id          bmt-plugin-addrbar
// @name        BMT: address bar tools
// @version     0.1337.0
// @namespace   BMT
// @updateURL   none
// @downloadURL none
// @description URL tools
// @author      Hedger
// @include     https://battlemap.deltatgame.com/*
// @match       https://battlemap.deltatgame.com/*
// @grant       none
// ==/UserScript==

function wrapper(plugin_info) {
// ensure plugin framework is there, even if iitc is not yet loaded
if(typeof window.plugin !== 'function') window.plugin = function() {};

// PLUGIN START ////////////////////////////////////////////////////////


// use own namespace for plugin
var thisplugin = window.plugin.addrBar = function() {};

thisplugin.viewURL = function() {
	var c = map.getCenter();
	var lat = Math.round(c.lat*1E6)/1E6;
	var lng = Math.round(c.lng*1E6)/1E6;
	var qry = 'll='+lat+','+lng+'&z=' + map.getZoom();

	return qry;
};

thisplugin.getUrlParameter = function(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

thisplugin.updateMap = function() {
	history.replaceState(null, null, "/home?" + thisplugin.viewURL());
};

thisplugin.setup  = function() {
    window.map.on('moveend', thisplugin.updateMap, null);
    var location = thisplugin.getUrlParameter('ll');
    if (location) {
    	latlng = location.split(',');
    	if (latlng.length != 2)
    		return;

    	window.map.setView(new window.L.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1])),
    		parseInt(thisplugin.getUrlParameter('z')));
    }
};

var setup = thisplugin.setup;

// PLUGIN END //////////////////////////////////////////////////////////


setup.info = plugin_info; //add the script info data to the function as a property
if(!window.bootPlugins) window.bootPlugins = [];
window.bootPlugins.push(setup);
// if we have already booted, immediately run the 'setup' function
if(window.bmtLoaded && typeof setup === 'function') { setup(); }
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);
