// ==UserScript==
// @id          bmt-loader
// @name        BMT Main script
// @version     0.1337.0
// @namespace   BMT
// @updateURL   none
// @downloadURL none
// @description Initializes plugin framework
// @author      Hedger
// @include     https://battlemap.deltatgame.com/*
// @match       https://battlemap.deltatgame.com/*
// @grant       none
// ==/UserScript==

function wrapper(plugin_info) {
// ensure plugin framework is there, even if iitc is not yet loaded
window.bmtLoaded = false;
if(typeof window.plugin !== 'function') window.plugin = function() {};
if(!window.bootPlugins) window.bootPlugins = [];

function doPluginInit() {
	for(var idx in window.bootPlugins) {
		var plugin = window.bootPlugins[idx];
		console.log('BMT: Processing plugin', bootPlugins);
		plugin();
	}
	window.bmtLoaded = true;
	console.log('BMT: loaded!');
}

console.log('BMT: init!');
$(window).off('load').on('load', function(){
	$('#page_loader').hide();
	doPluginInit();
});
} // wrapper end

// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);

